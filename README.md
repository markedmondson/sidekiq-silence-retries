# Sidekiq::SilenceRetries

Sidekiq::SilenceRetries is a middleware for Sidekiq that adds the ability to
silence specific exceptions for a certain retry count threshold. This is ideal
when a race condition might exist, for example using a state machine, where
the concurrency and execution order of multiple asynchronous jobs cannot be
guaranteed.

## Compatibility

Sidekiq::SilenceRetries supports Sidekiq versions 2 and 3 and is actively tested
against Ruby versions 2.0.0, 2.1, and 2.2.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'sidekiq-silence-retries'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sidekiq-silence-retries

## Configuration

In a Rails initializer add Sidekiq::SilenceRetries to your server middleware:

```ruby
Sidekiq.configure_server do |config|
  config.server_middleware do |chain|
    chain.add Sidekiq::SilenceRetries
  end
end
```

## Basic Usage

In a worker, specify a threshold (maximum retries) for silencing:

```ruby
class MyWorker
  include Sidekiq::Worker

  sidekiq_options silence: { exception: SomeException, threshold: 5 }

  def perform(args)
    # Worker code
  end
end
```

In this example, the job will execute normally, if the exception is raised, it
will be swallowed and retried until 5 retries have been performed.


## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).


## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
