require 'sidekiq'
require 'sidekiq/silence_retries/version'

module Sidekiq
  ##
  # Sidekiq server middleware. Silences retries of the set exception class up
  # to the set threshold count
  class SilenceRetries
    DEFAULT_THRESHOLD = 5

    # The default number of silenced retry attempts is 5. You can pass a default
    # value for this when adding the middleware using the options hash:
    #
    #   Sidekiq.configure_server do |config|
    #     config.server_middleware do |chain|
    #       chain.add Sidekiq::SilenceRetreies, threshold: 10
    #     end
    #   end

    def initialize(options = {})
      @threshold = options.fetch(:threshold, DEFAULT_THRESHOLD)
    end

    # Silence exceptions when the gem options are set
    # only when the number of retries is under the set_max_silences threshold
    def call(worker, msg, _queue)
      @worker_klass = worker.class

      yield
    rescue Exception => e
      # Re-raise any exception not defined in the options
      raise e unless silence_retries? &&
                     e.class == exception

      max_silence_attempts = retry_silences_from(msg['retry'], threshold)

      count = msg['retry_count'] || 0
      raise e unless count < max_silence_attempts
    end

    private

    def retry_silences_from(msg_retry, default)
      msg_retry.is_a?(Fixnum) ? msg_retry : default
    end

    def silence_retries?
      !exception.nil? && !threshold.nil?
    end

    def silence_options
      @worker_klass.get_sidekiq_options['silence'] || {}
    end

    def exception
      silence_options.fetch(:exception, nil)
    end

    def threshold
      silence_options.fetch(:threshold, @threshold)
    end
  end
end
