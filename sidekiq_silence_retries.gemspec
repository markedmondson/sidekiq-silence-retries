# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'sidekiq/silence_retries/version'

Gem::Specification.new do |spec|
  spec.name          = 'sidekiq_silence_retries'
  spec.version       = Sidekiq::SilenceRetries::VERSION
  spec.authors       = ['Mark Edmondson']
  spec.email         = ['mark.edmondson@gmail.com']

  spec.summary       = 'Silence retries of Sidekiq workers'
  spec.description   = 'Give Sidekiq workers the option to silence a configurable exception for a threshold number of retries'
  spec.homepage      = 'http://markedmondson.ca'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport'
  spec.add_dependency 'sidekiq', '>= 2.5', '< 5.0'

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'appraisal'
end
