class SomeClass
  class SomeException < StandardError; end

  def some_exception
    raise SomeClass::SomeException, 'Some exception'
  end
end
