require 'sidekiq'
require 'sidekiq/util'
require 'sidekiq/testing'

Sidekiq.logger.level = Logger::WARN
