class SilencedWorker
  include Sidekiq::Worker

  sidekiq_options silence: { exception: SomeClass::SomeException, threshold: 5 }

  def perform
    SomeClass.new.some_exception
  end

  def another_exception
    raise StandardError, 'Another exception'
  end
end
