class InvalidWorker
  include Sidekiq::Worker

  def perform
    SomeClass.new.some_exception
  end
end
