require 'spec_helper'

require 'workers/invalid_worker'
require 'workers/silenced_worker'

describe Sidekiq::SilenceRetries, sidekiq: :inline do
  it 'has a version number' do
    expect(Sidekiq::SilenceRetries::VERSION).not_to be nil
  end

  let(:middleware) { described_class.new }

  describe '.call' do
    context 'for a valid worker' do
      let(:worker) { InvalidWorker.new }

      it 'does not catch the exception' do
        expect { middleware.call(worker, {}, nil) { worker.perform } }.to raise_error SomeClass::SomeException
      end
    end

    context 'for a configured silenced worker' do
      let(:worker) { SilencedWorker.new }

      it 'silences an exception for SomeClass::SomeException' do
        expect { middleware.call(worker, {}, nil) { worker.perform } }.not_to raise_error
      end

      context 'for another exception' do
        it 'does not silence' do
          expect { middleware.call(worker, {}, nil) { worker.another_exception } }.to raise_error StandardError
        end
      end

      context 'when the retry threshold is not met' do
        it 'silences an exception for SomeClass::SomeException' do
          expect { middleware.call(worker, { 'retry_count' => 2 }, nil) { worker.perform } }.not_to raise_error
        end
      end

      context 'when the retry threshold is met' do
        it 'throws the SomeClass::SomeException exception' do
          expect { middleware.call(worker, { 'retry_count' => 6 }, nil) { worker.perform } }.to raise_error SomeClass::SomeException
        end
      end
    end
  end
end
