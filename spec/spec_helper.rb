$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'pry'
require 'sidekiq/silence_retries'

Dir["#{File.dirname(__FILE__)}/support/**/*.rb"].each { |f| require f }

unless ENV['CI']
  require 'simplecov'
  SimpleCov.configure do
    add_filter '/spec/'
  end
end
